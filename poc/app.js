const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

// Custom Modules
const index = require('./routes/index');
const schedule = require('./routes/scheduler/scheduler');
const calendar = require('./routes/scheduler/calendar');
const workers = require('./routes/resources/workers');
const resources = require('./routes/resources/resources');
app.use('/', index);
app.use('/schedule', schedule);
app.use('/calendar', calendar);
app.use('/resources', resources);
app.use('/workers', workers);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  console.log(err);

  // render the error page
  res.status(err.status || 500);
  return res.send(err);
});

module.exports = app;
