const express = require('express');
const router = express.Router();
const Resources = require('./../../modules/v1/resources/resources');
const _ = require('lodash');

router.get('/:id?', (req, res, next) => {
  let data = {};
  let query = {};

  const { id } = req.params;
  if (id) {
    query.id = id;
  }

  data = Resources.fetchResources(query);

  res.status(200);
  return res.send(data);
});

module.exports = router;
