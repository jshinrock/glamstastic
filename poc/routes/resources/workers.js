const express = require('express');
const router = express.Router();
const Workers = require('./../../modules/v1/resources/workers');
const _ = require('lodash');

router.get('/:id?', (req, res, next) => {
  let data = {};
  let query = {};

  const { id } = req.params;
  if (id) {
    query.id = id;
  }

  data = Workers.fetchWorkers(query);

  res.status(200);
  return res.send(data);
});

module.exports = router;
