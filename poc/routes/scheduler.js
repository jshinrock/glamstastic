const express = require('express');
const router = express.Router();
const _ = require('lodash');

router.get('/:id?', (req, res, next) => {
  let data = {};
  let query = { type: 'event' };

  const { id } = req.params;

  if (id) {
    query = _.assignIn(query, { techRef: id });
  }

  data = {
    query: query,
    id: id
  };

  return res
           .status(200)
           .send(data);
});

router.get()

module.exports = router;
