var express = require('express');
var router = express.Router();

router.get('/', (req, res, next) => {
  return res
           .status(200)
           .send('OK');
});

router.route('/healthcheck', (req, res) => {
  return res
           .status(200)
           .send('OK');
})

module.exports = router;
