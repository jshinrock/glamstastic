const express = require('express');
const router = express.Router();
const Events = require('./../../modules/v1/scheduler/events');
const _ = require('lodash');

router.get('/:id?', (req, res, next) => {
  let data = {};
  let query = {};

  const { id } = req.params;
  const { techRef, date } = req.query;

  query = _.extend(query, {id: id}, {techRef: techRef}, {date: date});

  data = Events.fetchEvents(query);

  res.status(200);
  return res.send(data);
});

router.post('/save', (req, res, next) => {
  const {event} = req.body;
  if (!event) {
    res.status(503);
    return res.send({error: 'Event does not have required params.'});
  }

  Events.saveEvent(event);
  res.status(200);
  return res.send({data: `Added ${event.length} events`});
});

module.exports = router;
