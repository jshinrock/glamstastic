const express = require('express');
const router = express.Router();
const Calendar = require('./../../modules/v1/scheduler/calendar');
const _ = require('lodash');

router.get('/weeks', (req, res, next) => {
  let date = req.query.date;
  if (!date) {
    res.status(503);
    return res.send({error: 'cannot parse date'});
  }
  let data = {};
  data.data = Calendar.getWeekSelection(date);

  res.status(200);
  return res.send(data);
});

router.get('/days', (req, res, next) => {
  let date = req.query.date;
  if (!date) {
    res.status(503);
    return res.send({error: 'cannot parse date'});
  }
  let data = {};
  data.data = Calendar.getDaySelection(date);

  res.status(200);
  return res.send(data);
});

router.get('/months', (req, res, next) => {
  let data = {};
  data.data = Calendar.getMonthSelection();

  res.status(200);
  return res.send(data);
});

module.exports = router;
