const _ = require('lodash');
const Workers = require('./workers');

const mockData = [
    {
        _id: 1,
        name: 'Cartography',
        workers: [1,2],
    },
    {
        _id: 2,
        name: 'Realestate planning',
        workers: [3]
    }
];

module.exports = {
    fetchResource: function (resource) {
        return _.find(mockData, r => String(r._id) === String(resource));
    },

    fetchResources: function (query) {
        let resources = mockData;
        if (query.id) {
            resources = this.fetchResource(query.id);
        }
        resources = this.fetchWorkersByResource(resources);
        return resources;
    },

    fetchWorkersByResource: function (resources) {
        if (Array.isArray(resources)) {
            return _.map(resources, resource => {
                resource = this.fetchWorkerByResource(resource);
                return resource;
            });
        }
        return this.fetchWorkerByResource(resources);
    },

    fetchWorkerByResource: function (resource) {
        const query = {
            id: resource.workers
        };
        resource.people = Workers.fetchWorkers(query);
        return _.omit(resource, 'workers');
    },

    saveResource: function (worker) {
        if (worker._id) {
            // @TODO update resource
        }
        // @TODO save new resource
    },
};
