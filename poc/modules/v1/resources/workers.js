const _ = require('lodash');

const mockData = [
    {
        _id: 1,
        name: 'Buster Bluth',
    },
    {
        _id: 3,
        name: 'GOB Bluth',
    },
    {
        _id: 2,
        name: 'Franklin Bluth',
    },
];

module.exports = {
    fetchWorker: function (worker) {
        if (Array.isArray(worker)) {
            return _.filter(mockData, w => _.includes(worker, w._id));
        }
        return _.find(mockData, w => String(w._id) === String(worker));
    },

    fetchWorkers: function (query) {
        if (query.id) {
            return this.fetchWorker(query.id);
        }
        return mockData;
    },

    saveWorker: function (worker) {
        if (worker._id) {
            // @TODO update user
        }
        // @TODO save new worker
    },
};
