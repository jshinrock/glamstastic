const _ = require('lodash');
const Moment = require('moment');

module.exports = {

    _dateObj: function (opts=false) {
        if (opts) {
            return Moment(opts);
        }
        return Moment();
    },

    makeDate: function (dateStr='') {
        return this._dateObj(dateStr);
    },

    getCurrentDate: function () {
        let date = this._dateObj();
        return date;
    },

    getCurrentYear: function (date) {
        const today = this.getCurrentDate();
        return today.format('YYYY');
    },

    datesAreEqual: function (date1, date2) {
        return Moment(date1).isSame(Moment(date2), 'day');
    }

};

