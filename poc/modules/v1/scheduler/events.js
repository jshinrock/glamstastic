const _ = require('lodash');
const Resources = require('./../resources/resources');

let mockDataResult = [
  {
    id: 1,
    date: '2018-01-31',
    startTime: 1516026000,
    endTime: 1516028400,
    servicePackageRef: 56,
    resourceRef: 2,
    customerRef: 2,
    status: 'approved'
  },
  {
    id: 4,
    date: '2018-01-31',
    startTime: 0,
    endTime: 0,
    servicePackageRef: 56,
    resourceRef: 1,
    customerRef: 3,
    status: 'approved'
  },
  {
    id: 2,
    date: '2018-01-15',
    startTime: 1516026000,
    endTime: 1516028400,
    servicePackageRef: 56,
    resourceRef: 1,
    customerRef: 2,
    status: 'approved'
  },
  {
    id: 3,
    date: '2018-01-08',
    startTime: 1516026000,
    endTime: 1516028400,
    servicePackageRef: 56,
    resourceRef: 2,
    customerRef: 2,
    status: 'approved'
  }
];

module.exports = {

    _canSave: function (event) {
      console.log(event);
      let canSave = false;
      try {
        const resource = Resources.fetchResource(event.resourceRef);
        const eventsForDay = this.fetchEvents({date: event.date});
        console.log('r', resource);
        console.log('e', eventsForDay);
      } catch (e) {
        console.log(e);
        canSave = false;
      }
      return canSave;
    },

    fetchEvents: function (filter={}) {
      console.log(filter);
      let data = mockDataResult;
      _.each(filter, (f, key) => {
        if (f) {
          data = data.filter(d => String(d[key]) === f);
        }
      });
      // if (filter.techRef) {
      //   data = data.filter(d => String(d.techRef) === filter.techRef);
      // }
      return data;
    },

    saveEvent: function (event) {
      if (event.id) {
        let found = _.find(mockDataResult, e => e.id === event.id);
        if (this._canSave(found)) {
          found = _.assign(found, event);
        }
        return;
      }
      const lastId = _.map(mockDataResult, 'id');
      event.id = _.last(lastId) + 1;
      if (this._canSave(event)) {
        mockDataResult.push(event);
      }
      return ;
    }
}
