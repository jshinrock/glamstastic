const Calendar = require('node-calendar');
const BaseDate = require('./../date/baseDate');
const Events = require('./../scheduler/events');
const _ = require('lodash');

module.exports = {

    _getCalendar: function() {
        return new Calendar.Calendar();
    },

    _findDateInRage: function (range, date) {
        const pointer = date;
        let index = null;
        _.forEach(range, month => {
            const flat = _.flattenDeep(month);
            const dates = _.map(flat, date => {
                let iso;
                if (date.date) {
                    date = date.date;
                }
                try {
                    iso = date.toISOString();
                } catch (e) {
                    return date;
                }
                return iso.split('T')[0];
            });
            const found = _.find(dates, date => date === pointer);
            if (found) {
                index = _.indexOf(range, month);
            }
        });

        if (index) {
            return index;
        }
        console.error('Calendar._findDateInRage: could not find %s in range', pointer);
        return null;
    },

    _getYearProgress: function (year, today) {
        const now = today.format('YYYY-MM-DD');
        let index = this._findDateInRage(year, now);
        return _.drop(year, index);;
    },

    _assignAppointmentsToDates: function (events, range) {
        const data = _.map(range, date => {
            const eventsForDate = _.filter(events, event => {
                return BaseDate.datesAreEqual(event.date, date);
            });
            const now = BaseDate.getCurrentDate().startOf('day');
            return {
                date: date,
                available: this._isInFuture(now, date),
                events: eventsForDate,
                today: this._isToday(now, date),
            };
        });
        return data;
    },

    _isInFuture: function (now, date) {
        const comparator = BaseDate.makeDate(date);
        return now.isSameOrBefore(comparator);
    },

    _isToday: function (now, date) {
        const comparator = BaseDate.makeDate(date);
        return now.isSame(comparator);
    },

    _deriveRangeBusyPercentage: function (events, range ) {
        const rangeAndEventData = this._assignAppointmentsToDates(events, range);
        // @TODO assuming 16 hours a day available, will need config
        return rangeAndEventData;
    },

    getYear: function() {
        const currentYear = BaseDate.getCurrentYear();
        const today = BaseDate.getCurrentDate();
        const cal = this._getCalendar();
        const yearStructure = cal.yeardatescalendar(currentYear, 1);
        const currentYearProgress = this._getYearProgress(yearStructure, today);
        return currentYearProgress;
    },

    getMonthSelection: function () {
        const year = this.getYear();
        const selections = _.map(year, month => {
            const flat = _.flattenDeep(month);
            const day = BaseDate.makeDate(flat[7]);
            return {
                id: day.format('M'),
                date: day.format('YYYY-MM-DD'),
                display: day.format('MMMM')
            }
        });
        const months = _.dropRight(selections, 6);
        return months;
    },

    getWeekSelection: function (date) {
        date = BaseDate.makeDate(date);
        const year = date.format('YYYY');
        const month = date.format('M');
        const cal = this._getCalendar();
        const weeks = cal.monthdatescalendar(year, month);
        const events = Events.fetchEvents();
        let data = _.map(weeks, week => {
            return this._deriveRangeBusyPercentage(events, week);
        });
        return data;
    },

    getDaySelection: function (date) {
        date = BaseDate.makeDate(date);
        const month = this.getWeekSelection(date);
        const index = this._findDateInRage(month, date.format('YYYY-MM-DD'));
        let data = _.nth(month, index);
        // @TODO filter by resource
        const events = Events.fetchEvents();
        data = this._assignAppointmentsToDates(events, data);
        return data;
    }
};

