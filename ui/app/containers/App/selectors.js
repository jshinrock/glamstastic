/**
 * The global state selectors
 */

import { createSelector } from 'reselect';

const selectGlobal = (state) => state.get('global');
const selectRoute = (state) => state.get('route');


const makeSelectLoading = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('loading')
);

const makeSelectError = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('error')
);

const makeSelectLocation = () => createSelector(
  selectRoute,
  (routeState) => routeState.get('location').toJS()
);

const makeSelectMonths = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('calendar').toJS()
);

const makeSelectSelection = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('selected').toJS()
);


export {
  selectGlobal,
  makeSelectLoading,
  makeSelectError,
  makeSelectLocation,
  makeSelectMonths,
  makeSelectSelection,
};
