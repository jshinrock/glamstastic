/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const DEFAULT_LOCALE = 'en';
export const FETCH_MONTHS = 'containers/App/FETCH_MONTHS';
export const FETCH_MONTHS_SUCCESS = 'containers/App/FETCH_MONTHS_SUCCESS';
export const FETCH_MONTHS_ERROR = 'constainers/App/FETCH_MONTHS_ERROR';
export const FETCH_WEEKS = 'containers/App/FETCH_WEEKS';
export const FETCH_WEEKS_SUCCESS = 'containers/App/FETCH_WEEKS_SUCCESS';
export const FETCH_WEEKS_ERROR = 'constainers/App/FETCH_WEEKS_ERROR';
export const FETCH_DAYS = 'containers/App/FETCH_DAYS';
export const FETCH_DAYS_SUCCESS = 'containers/App/FETCH_DAYS_SUCCESS';
export const FETCH_DAYS_ERROR = 'constainers/App/FETCH_DAYS_ERROR';
