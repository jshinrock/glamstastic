/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  FETCH_MONTHS,
  FETCH_MONTHS_SUCCESS,
  FETCH_MONTHS_ERROR,
  FETCH_WEEKS,
  FETCH_WEEKS_SUCCESS,
  FETCH_WEEKS_ERROR,
  FETCH_DAYS,
  FETCH_DAYS_SUCCESS,
  FETCH_DAYS_ERROR,
} from './constants';

/**
 * Fetch months, this action starts the request saga
 *
 * @return {object} An action object with a type of FETCH_MONTHS
 */
export function fetchMonths() {
  return {
    type: FETCH_MONTHS,
  };
}

export function fetchWeeks(date) {
  return {
    type: FETCH_WEEKS,
    date,
  };
}

export function fetchDays(date) {
  return {
    type: FETCH_DAYS,
    date,
  };
}

/**
 * Dispatched when the months are loaded by the request saga
 *
 * @param  {array} repos The repository data
 * @param  {string} username The current username
 *
 * @return {object}      An action object with a type of FETCH_MONTHS_SUCCESS passing the repos
 */
export function monthsLoaded(months) {
  return {
    type: FETCH_MONTHS_SUCCESS,
    months,
  };
}

export function weeksLoaded(weeks, month) {
  return {
    type: FETCH_WEEKS_SUCCESS,
    weeks,
    month,
  };
}

export function daysLoaded(days) {
  return {
    type: FETCH_DAYS_SUCCESS,
    days,
  };
}

/**
 * Dispatched when loading the months fails
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of FETCH_MONTHS_ERROR passing the error
 */
export function monthsLoadingError(error) {
  return {
    type: FETCH_MONTHS_ERROR,
    error,
  };
}

export function weeksLoadingError(error) {
  return {
    type: FETCH_WEEKS_ERROR,
    error,
  };

}
export function daysLoadingError(error) {
  return {
    type: FETCH_DAYS_ERROR,
    error,
  };
}
