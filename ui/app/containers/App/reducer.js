/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import {
  FETCH_MONTHS,
  FETCH_MONTHS_SUCCESS,
  FETCH_MONTHS_ERROR,
  FETCH_WEEKS,
  FETCH_WEEKS_SUCCESS,
  FETCH_WEEKS_ERROR,
  FETCH_DAYS,
  FETCH_DAYS_SUCCESS,
  FETCH_DAYS_ERROR,
} from './constants';


// The initial state of the App
const initialState = fromJS({
  loading: false,
  error: false,
  calendar: {
    months: [],
    weeks: [],
    days: [],
  },
  selected: {
    month: null,
    week: null,
    day: null,
  },
  view: 'months',
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_MONTHS:
      return state
        .set('view', 'months')
        .set('loading', true)
        .set('error', false)
        .setIn(['calendar', 'months'], [])
        .setIn(['calendar', 'weeks'], [])
        .setIn(['calendar', 'days'], []);
    case FETCH_MONTHS_SUCCESS:
      return state
        .set('view', 'months')
        .set('loading', false)
        .setIn(['calendar', 'months'], action.months.data);
    case FETCH_MONTHS_ERROR:
      return state
        .set('view', 'months')
        .set('error', action.error)
        .set('loading', false);
    case FETCH_WEEKS:
      return state
        .set('view', 'weeks')
        .set('loading', true)
        .set('error', false)
        .setIn(['calendar', 'weeks'], [])
        .setIn(['calendar', 'days'], []);
    case FETCH_WEEKS_SUCCESS:
      return state
        .set('view', 'weeks')
        .set('loading', false)
        .setIn(['calendar', 'weeks'], action.weeks.data)
        .setIn(['selected', 'month'], action.month);
    case FETCH_WEEKS_ERROR:
      return state
        .set('view', 'months')
        .set('error', action.error)
        .set('loading', false);
    case FETCH_DAYS:
      return state
        .set('view', 'days')
        .set('loading', true)
        .set('error', false)
        .setIn(['calendar', 'days'], []);
    case FETCH_DAYS_SUCCESS:
      return state
        .set('view', 'days')
        .set('loading', false)
        .setIn(['calendar', 'days'], action.days.data);
    case FETCH_DAYS_ERROR:
      return state
        .set('view', 'weeks')
        .set('error', action.error)
        .set('loading', false);
    default:
      return state;
  }
}

export default appReducer;
