/**
 * Gets the repositories of the user from Github
 */

import { call, put, takeLatest, all } from 'redux-saga/effects';
import { FETCH_MONTHS, FETCH_WEEKS, FETCH_DAYS } from 'containers/App/constants';
import {
  monthsLoaded,
  monthsLoadingError,
  weeksLoaded,
  weeksLoadingError,
  daysLoaded,
  daysLoadingError,
   } from 'containers/App/actions';

import request from 'utils/request';

export function* fetchMonths() {
  const requestURL = 'http://localhost:3003/calendar/months';

  try {
    const months = yield call(request, requestURL);
    yield put(monthsLoaded(months));
  } catch (err) {
    yield put(monthsLoadingError(err));
  }
}

export function* fetchWeeks(action) {
  const requestURL = `http://localhost:3003/calendar/weeks?date=${action.date}`;

  try {
    const weeks = yield call(request, requestURL);
    yield put(weeksLoaded(weeks, action.date));
  } catch (err) {
    yield put(weeksLoadingError(err));
  }
}

export function* fetchDays(action) {
  try {
    const requestURL = `http://localhost:3003/calendar/days?date=${action.date}`;
    const days = yield call(request, requestURL);
    yield put(daysLoaded(days));
  } catch (err) {
    yield put(daysLoadingError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* dateData() {
// export default function* getMonthsData() {
  // Watches for FETCH_MONTHS actions and calls fetchMonths when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount

  // yield takeLatest(FETCH_MONTHS, fetchMonths);
  yield all([
    takeLatest(FETCH_MONTHS, fetchMonths),
    takeLatest(FETCH_WEEKS, fetchWeeks),
    takeLatest(FETCH_DAYS, fetchDays),
  ]);
}
