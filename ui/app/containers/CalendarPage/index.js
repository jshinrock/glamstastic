/*
 * CalendarPage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

// package deps
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

// view components
import Wrapper from 'components/AppWrapper';
import GenericUl from 'components/Generic/ul';
import Months from 'components/Month';
import Weeks from 'components/Week';
import Days from 'components/Day';

// custom logic
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { makeSelectLoading, makeSelectError, makeSelectMonths, makeSelectSelection } from '../App/selectors';
import { fetchMonths, fetchWeeks, fetchDays } from '../App/actions';
import reducer from './reducer';
import saga from './saga';


export class CalendarPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {
    // fetch months on initial load
    this.props.fetchMonths();
  }

  render() {
    const { loading, error, calendar, view, selected } = this.props;

    const monthsListProps = {
      loading,
      error,
      months: calendar.months,
      onClickEvent: this.props.onSelectMonth,
    };
    const weeksListProps = {
      loading,
      error,
      weeks: calendar.weeks,
      onClickEvent: this.props.onSelectDay,
      selected,
      onBackEvent: this.props.fetchMonths,
    };
    const daysListProps = {
      loading,
      error,
      days: calendar.days,
      selected,
      onBackEvent: this.props.fetchMonths,
    };

    const monthsList = (<Months {...monthsListProps} />);
    const weeksList = (<Weeks {...weeksListProps} />);
    const daysList = (<Days {...daysListProps} />);
    let content;

    if (calendar.days.length) {
      content = daysList;
    } else if (calendar.weeks.length) {
      content = weeksList;
    } else {
      content = monthsList;
    }

    return (
      <Wrapper>
        {content}
      </Wrapper>
    );

  }
}

CalendarPage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  calendar: PropTypes.object,
  selected: PropTypes.object,
  view: PropTypes.string,
  onSelectMonth: PropTypes.func,
  onSelectDay: PropTypes.func,
  onBack: PropTypes.func,
  fetchMonths: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    onSelectMonth: (event) => {
      if (event !== undefined && event.preventDefault) {
        event.preventDefault();
      }
      dispatch(fetchWeeks(event.date));
    },
    onSelectDay: (event) => {
      if (event !== undefined && event.preventDefault) {
        event.preventDefault();
      }
      dispatch(fetchDays(event.date));
    },
    fetchMonths: () => {
      dispatch(fetchMonths());
    },
  };
}

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  error: makeSelectError(),
  calendar: makeSelectMonths(),
  selected: makeSelectSelection(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({ key: 'calendar', reducer });
const withSaga = injectSaga({ key: 'calendar', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(CalendarPage);
