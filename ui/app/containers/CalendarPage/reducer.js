/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';

import {
  SELECT_MONTH,
} from './constants';

// The initial state of the App
const initialState = fromJS({
});

function calendarReducer(state = initialState, action) {
  switch (action.type) {
    case SELECT_MONTH:
      return state
        .set('calendar', action.name);
    default:
      return state;
  }
}

export default calendarReducer;
