/**
 *
 * Month.js
 *
 * A single month on the scheduler calendar
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import MonthView from './MonthView';
import GenericUl from './../Generic/ul';
import H2Label from './../Generic/h2Label';

function Month({ loading, error, months, onClickEvent }) {
  // Render a month

  if (loading) {
    return (<h4>loading...</h4>);
  }

  if (error !== false) {
    return (<h4>error...</h4>);
  }


  if (months !== false) {
    const content = months.map((month) => {
      const monthProps = {
        month,
        onClickEvent,
      };
      return (<MonthView {...monthProps} />);
    });

    return (
      <div>
        <H2Label>Select month</H2Label>
        <GenericUl className="calender months">
          {content}
        </GenericUl>
      </div>
    );
  }

  return null;
}

function handleClick() {

}

Month.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.any,
  months: PropTypes.any,
  onClickEvent: PropTypes.func,
  handleClick: PropTypes.func,
};

export default Month;
