/**
 *
 * Month.js
 *
 * A single month on the scheduler calendar
 *
 */

import styled from 'styled-components';

const MonthHeader = styled.div`
  padding: 1rem;
  background: red;
  color: #fff;
  width: 100%;
  font-weight: bold;
`;

export default MonthHeader;
