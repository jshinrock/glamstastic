/**
 *
 * Month.js
 *
 * A single month on the scheduler calendar
 *
 */

import styled from 'styled-components';

const MonthBox = styled.div`
  border: 1px solid #ccc;
  border-radius: 3px;
  min-height: 145px;
`;

export default MonthBox;
