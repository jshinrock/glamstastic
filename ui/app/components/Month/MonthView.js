/**
 *
 * Month.js
 *
 * A single month on the scheduler calendar
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import MonthWrapper from './MonthWrapper';
import MonthHeader from './MonthHeader';
import MonthBox from './MonthBox';

function Month({ month, onClickEvent }) {
  // Render a month
  return (
    <MonthWrapper className="month" key={month.id} onClick={() => onClickEvent({date: month.date})}>
      <MonthBox>
        <MonthHeader>
          {month.display}
        </MonthHeader>
      </MonthBox>
    </MonthWrapper>
  );
}


Month.propTypes = {
  month: PropTypes.any,
  onClickEvent: PropTypes.func,
};

export default Month;
