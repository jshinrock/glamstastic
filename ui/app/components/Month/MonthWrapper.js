/**
 *
 * Month.js
 *
 * A single month on the scheduler calendar
 *
 */

import styled from 'styled-components';

const MonthWrapper = styled.li`
  padding: 1px;
  display: inline-block;
  *display: inline;
  zoom: 1;
  vertical-align: top;
  width: 20%;
  min-height:150px;
  cursor: pointer;
  background: #fff;
  transform: scale(0.95);
  filter: grayscale(100%);
  transition: filter .4s ease;
  transition: transform .4s ease;
  :hover {
  	transform: scale(1);
    filter: grayscale(0%);
    transition: transform .5s ease;
    transition: filter .15s ease;
  }
`;

export default MonthWrapper;
