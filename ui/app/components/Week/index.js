/**
 *
 * Week.js
 *
 * Week view on the scheduler calendar
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import WeekView from './WeekView';
import WeekLabel from './WeekLabel';
import H2Label from './../Generic/h2Label';
import GenericUl from './../Generic/ul';
import GenericBack from './../Generic/back';
import MonthMap from './MonthMap';

function Week({ loading, error, selected, weeks, onClickEvent, onBackEvent }) {
  // Render a month

  if (loading) {
    return (<h4>loading...</h4>);
  }

  if (error !== false) {
    return (<h4>error...</h4>);
  }

  if (weeks !== false) {
    const content = weeks.map((week) => {
      const isAvailable = isWeekAvailable(week);
      const weekProps = {
        week,
        onClickEvent,
        selected,
        available: isAvailable,
      };
      return (<WeekView {...weekProps} />);
    });

    const month = parseMonth(selected.month);

    return (
      <div>
        <H2Label>
          {month.display}
          <GenericBack onClick={onBackEvent}>Back</GenericBack>
        </H2Label>
        <GenericUl className="calender weeks">
          {content}
        </GenericUl>
      </div>
    );
  }

  return null;
}

function parseMonth(month) {
  const date = new Date(month);
  return MonthMap.find((m) => m.id === date.getMonth());
}

function isWeekAvailable(week) {
  let available = false;
  const reducer = (day) => {
    return day.available;
  };
  available = week.every(reducer);
  return available;
}

Week.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.any,
  weeks: PropTypes.any,
  selected: PropTypes.object,
  onClickEvent: PropTypes.func,
  onBackEvent: PropTypes.func,
};

export default Week;
