/**
 *
 * Week.js
 *
 * A single month on the scheduler calendar
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import WeekWrapper from './WeekWrapper';
import WeekLabel from './WeekLabel';
import WeeKWithDays from './WeeKWithDays';
import SmallDayView from './../Day/SmallDayView';
import DayHeader from './../Day/DayHeader';
import MonthMap from './MonthMap';

function WeekView({ week, onClickEvent, available }) {
  // Render a week
  const days = week.map((day) => {
    const dateDisplay = constructDateDisplay(day.date);
    return (
      <SmallDayView>
        <DayHeader className={(day.available ? '' : 'disabled')}>
          {dateDisplay}
        </DayHeader>
      </SmallDayView>
    );
  });

  const weekLabel = constructDateDisplay(week[0].date);
  return (
    <WeekWrapper className={"week " + (available ? '' : 'disabled')} key={week[0].date} onClick={() => onClickEvent({ date: week[0].date })}>
      <WeeKWithDays>
        {days}
      </WeeKWithDays>
    </WeekWrapper>
  );
}

function constructDateDisplay (date) {
  let dateDisplay = new Date(date);
  let month = parseMonth(dateDisplay.getMonth());
  return `${month.label}, ${dateDisplay.getDate()}`;
}

function parseMonth(month) {
  return MonthMap.find((m) => m.id === month);
}

WeekView.propTypes = {
  week: PropTypes.any,
  onClickEvent: PropTypes.func,
};

export default WeekView;
