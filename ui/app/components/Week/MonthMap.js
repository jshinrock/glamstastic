// const monthMap = [
//   { id: 0, label: 'January' },
//   { id: 1, label: 'February' },
//   { id: 2, label: 'March' },
//   { id: 3, label: 'April' },
//   { id: 4, label: 'May' },
//   { id: 5, label: 'June' },
//   { id: 6, label: 'July' },
//   { id: 7, label: 'August' },
//   { id: 8, label: 'September' },
//   { id: 9, label: 'October' },
//   { id: 10, label: 'November' },
//   { id: 11, label: 'December' },
// ];
const monthMap = [
  { id: 0, label: 'JAN', display: 'January' },
  { id: 1, label: 'FEB', display: 'February' },
  { id: 2, label: 'MAR', display: 'March' },
  { id: 3, label: 'APR', display: 'April' },
  { id: 4, label: 'MAY', display: 'May' },
  { id: 5, label: 'JUN', display: 'June' },
  { id: 6, label: 'JUL', display: 'July' },
  { id: 7, label: 'AUG', display: 'August' },
  { id: 8, label: 'SEP', display: 'September' },
  { id: 9, label: 'OCT', display: 'October' },
  { id: 10, label: 'NOV', display: 'November' },
  { id: 11, label: 'DEC', display: 'December' },
];

export default monthMap;
