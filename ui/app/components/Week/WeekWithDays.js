/**
 *
 * Week.js
 *
 * A single month on the scheduler calendar
 *
 */

import styled from 'styled-components';

const WeekWrapper = styled.ul`
  padding: 0rem;
`;

export default WeekWrapper;
