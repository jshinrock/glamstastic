/**
 *
 * Week.js
 *
 * A single month on the scheduler calendar
 *
 */

import styled from 'styled-components';

const WeekWrapper = styled.li`
  padding: 5px 0;
  cursor: pointer;
  list-style-type: none;
  transform: scale(0.98);
  filter: grayscale(100%) brightness(100%);
  transition: filter 1s linear;
  transition: transform .4s ease;
  color: #fff;
  :hover {
    transform: scale(1);
    filter: grayscale(0%) brightness(100%);
    transition: transform .1s ease;
    transition: filter .12s linear;
  }
`;

export default WeekWrapper;
