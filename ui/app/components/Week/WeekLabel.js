/**
 *
 * Week.js
 *
 * A single month on the scheduler calendar
 *
 */

import styled from 'styled-components';

const WeekLabel = styled.h3`
  width: 100%;
  text-align: left;
  color: #ccc;
`;

export default WeekLabel;
