import { css } from 'styled-components';

const dayStyles = css`
  display: inline-block;
  box-sizing: border-box;
  padding: 1px;
  width: 100%;
  min-height: 100px;
  list-style-type: none;
  :after {
    content: '',
    width: 100%;
    display: inline-block;
  }
`;

export default dayStyles;
