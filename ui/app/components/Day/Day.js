import styled from 'styled-components';

import dayStyles from './dayStyles';

const Day = styled.div`${dayStyles}`;

export default Day;
