/**
 *
 * Day.js
 *
 * A single day on the scheduler calendar
 *
 */

import React, { Children } from 'react';
import PropTypes from 'prop-types';

import DateDisplay from './DateDisplay';
import DayWrapper from './DayWrapper';
import Day from './DayView';
import H2Label from './../Generic/h2Label';
import GenericUl from './../Generic/ul';
import GenericBack from './../Generic/back';
import MonthMap from './../Week/MonthMap';

function DayDate({ loading, error, selected, days, onBackEvent }) {
  // Render a day

  if (loading) {
    return (<h4>loading...</h4>);
  }

  if (error !== false) {
    return (<h4>error...</h4>);
  }

  if (days !== false) {
    const content = days.map((day) => {
      const dayProps = {
        day,
        selected,
      };
      return (<Day {...dayProps} />);
    });

    const dayDisplay = parseDay(days[0].date.date);
    const monthDisplay = parseMonth(days[0].date.date);

    return (
      <div>
        <H2Label>
          Week of {monthDisplay.display} {dayDisplay}
          <GenericBack onClick={onBackEvent}>Back</GenericBack>
        </H2Label>
        <GenericUl className="calender days">
          {content}
        </GenericUl>
      </div>
    );
  }

  return null;
}

function parseDay(date) {
  let day = new Date(date);
  return day.getDate();
}

function parseMonth(month) {
  const date = new Date(month);
  return MonthMap.find((m) => m.id === date.getMonth());
}

DayDate.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.any,
  days: PropTypes.array,
  selected: PropTypes.object,
  onBackEvent: PropTypes.func,
};

export default DayDate;
