/**
 *
 * Day.js
 *
 * A single day on the scheduler calendar
 *
 */

import styled from 'styled-components';

const DayWrapper = styled.li`
  padding: 0;
  display: inline-block;
  width: 14%;
  list-style-type: none;
  margin-right: 2px;
  :last-of-type {
    margin: 0;
  }
  cursor: pointer;
  font-size: 80%;
  transform: scale(0.95);
  filter: grayscale(100%);
  transition: scale .4s ease;
  transition: filter .2s linear;
  :hover {
    transform: scale(1);
    transition: transform .1s ease;
    filter: grayscale(0%);
    transition: filter .12s ease;
  }
`;

export default DayWrapper;
