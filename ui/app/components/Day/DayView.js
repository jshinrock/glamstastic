/**
 *
 * Week.js
 *
 * A single month on the scheduler calendar
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import DayWrapper from './DayWrapper';
import Day from './Day';
import DayBox from './DayBox';
import DayHeader from './DayHeader';
import MonthMap from './../Week/MonthMap';

function DayView({ day }) {
  const display = constructDateDisplay(day.date.date);
  // Render a day
  return (
    <DayWrapper className="day" key={day.date.date}>
      <DayBox>
        <DayHeader>
          {display}
        </DayHeader>
        <Day />
      </DayBox>
    </DayWrapper>
  );
}

function constructDateDisplay (date) {
  let dateDisplay = new Date(date);
  let month = parseMonth(dateDisplay.getMonth());
  return `${month.label}, ${dateDisplay.getDate()}`;
}

function parseMonth(month) {
  return MonthMap.find((m) => m.id === month);
}

DayView.propTypes = {
  day: PropTypes.any,
};

export default DayView;
