import styled from 'styled-components';

const DateDisplay = styled.p`
  font-weight: bold;
`;

export default DateDisplay;
