import styled from 'styled-components';

const DayBox = styled.div`
  border: 1px solid #ccc;
  border-radius: 3px;
  color: #fff;
`;

export default DayBox;
