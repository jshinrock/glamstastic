/**
 *
 * Day.js
 *
 * A single day on the scheduler calendar
 *
 */
import styled from 'styled-components';

const DayHeader = styled.div`
  background: red;
  border-bottom: 1px solid #ccc;
  width: 100%;
  color: inherit;
  padding: .75rem;
  &.disabled {
    background: #ccc;
  }
`;

export default DayHeader;
