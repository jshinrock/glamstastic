/**
 *
 * Day.js
 *
 * A single day on the scheduler calendar
 *
 */
import styled from 'styled-components';

const SmallDayView = styled.li`
  display: inline-block;
  border: 1px solid #ccc;
  width: 14%;
  color: inherit;
  font-size: 80%;
  list-style-type: none;
  min-height: 100px;
  border-radius: 3px;
  margin-right: 2px;
  :last-of-type {
    margin: 0
  }
`;

export default SmallDayView;
