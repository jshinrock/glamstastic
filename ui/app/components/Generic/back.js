/**
 *
 * Month.js
 *
 * A single month on the scheduler calendar
 *
 */

import styled from 'styled-components';

const GenericUl = styled.button`
  padding: 0;
  margin: 0;
  font-size: 14px;
  color #ccc;
  cursor: pointer;
  float: right;
  margin-top: 1rem;
`;

export default GenericUl;
