/**
 *
 * Label.js
 *
 * A generic H2 label
 *
 */

import styled from 'styled-components';

const H2Label = styled.h2`
  padding: 3rem 0 1rem 0;
  margin: 0 0 50px 0;
  border-bottom: 1px solid #ccc;
`;

export default H2Label;
