/**
 *
 * Month.js
 *
 * A single month on the scheduler calendar
 *
 */

import styled from 'styled-components';

const GenericUl = styled.ul`
  padding: 0;
  margin: 0;
`;

export default GenericUl;
