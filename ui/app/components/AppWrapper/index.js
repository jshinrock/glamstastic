/**
 *
 * Month.js
 *
 * A single month on the scheduler calendar
 *
 */
import styled from 'styled-components';


const Wrapper = styled.div`
  min-width: 500px;
  margin: 3rem auto 0 auto;
  max-width: 800px;
  text-align: left;
  border: 1px solid #ccc;
  padding: 5rem;
  border-radius: 5px;
  @import url('https://fonts.googleapis.com/css?family=Montserrat');
  font-family: 'Montserrat', sans-serif;
  background: #fff;
`;

export default Wrapper;
